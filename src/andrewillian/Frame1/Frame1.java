package andrewillian.Frame1;

import java.awt.BorderLayout;

import javax.media.opengl.GLCanvas;
import javax.media.opengl.GLCapabilities;
import javax.swing.JFrame;
import javax.swing.WindowConstants;
import javax.media.opengl.GL;

public class Frame1 extends JFrame{

	public static Frame1 that;
	GLCanvas canvas;
	
	private static final long serialVersionUID = 1L;
	private Main renderer = new Main();
	
	private int janelaLargura  = 400, janelaAltura = 400;

	
	public Frame1() {		
		// Cria o frame.
		super("Exercicio 1");   
		setBounds(300,250,janelaLargura,janelaAltura+22);  // 500 + 22 da borda do t�tulo da janela
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		getContentPane().setLayout(new BorderLayout());

		/* Cria um objeto GLCapabilities para especificar 
		 * o numero de bits por pixel para RGBA
		 */
		GLCapabilities glCaps = new GLCapabilities();
		glCaps.setRedBits(8);
		glCaps.setBlueBits(8);
		glCaps.setGreenBits(8);
		glCaps.setAlphaBits(8); 

		/* Cria um canvas, adiciona ao frame e objeto "ouvinte" 
		 * para os eventos Gl, de mouse e teclado
		 */
		canvas = new GLCanvas(glCaps);
		add(canvas,BorderLayout.CENTER);
		canvas.addGLEventListener(renderer);        
		canvas.addKeyListener(renderer);
		canvas.requestFocus();			
	}		
	
	public static void main(String[] args) {
		that = new Frame1();
		that.setVisible(true);
		int a = 0;
	}
	
}
